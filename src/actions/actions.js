export const setPreloader = (isShown) => ({
  type: "SET_PRELOADER",
  payload: {
    isShown,
  },
});

export const setMessages = (messages) => ({
  type: "SET_MESSAGES",
  payload: {
    messages,
  },
});

export const setUsers = (users) => ({
  type: "SET_USERS",
  payload: {
    users,
  },
});

export const setCurrentUserId = (id) => ({
  type: "SET_CURRENT_USER_ID",
  payload: {
    id,
  },
});

export const toggleEditModal = (isEdit) => ({
  type: "TOGGLE_EDIT_MODAL",
  payload: {
    isEdit,
  },
});

export const setEditingMessageId = (messageId) => ({
  type: "SET_EDITING_MESSAGE_ID",
  payload: {
    messageId,
  },
});
