import React from "react";
import { connect } from "react-redux";
import { v4 as uuidv4 } from "uuid";
import "./Chat.css";
import {
  setPreloader,
  setMessages,
  setUsers,
  setCurrentUserId,
} from "../actions/actions";
import Header from "./Header";
import MessageList from "./messages/MessageList";
import MessageInput from "./messages/MessageInput";
import Preloader from "./Preloader";
import EditMessageModal from "./messages/EditMessageModal";

class Chat extends React.Component {
  getMessages = async () => {
    const response = await fetch(this.props.url);
    const data = await response.json();
    this.props.setMessages(data);
    this.props.setPreloader(false);
    this.getUsersCount();
    return data;
  };

  getUsersCount = () => {
    const users = [];
    for (const message of this.props.chat.messages) {
      if (!users.includes(message.userId)) {
        users.push(message.userId);
      }
    }
    this.props.setUsers(users);
  };

  setUserId = () => {
    const id = uuidv4();
    this.props.setCurrentUserId(id);
  };

  componentDidMount = () => {
    this.getMessages();
    this.setUserId();
  };

  render() {
    if (this.props.chat.preloader) return <Preloader />;
    return (
      <div className="chat">
        <Header
          users={this.props.chat.users}
          messages={this.props.chat.messages}
        />
        {this.props.chat.editModal ? <EditMessageModal /> : null}
        <MessageList userId={this.props.chat.currentUserId} />
        <MessageInput userId={this.props.chat.currentUserId} />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    chat: state.chat,
  };
};

const mapDispatchToProps = {
  setPreloader,
  setMessages,
  setUsers,
  setCurrentUserId,
};

export default connect(mapStateToProps, mapDispatchToProps)(Chat);
