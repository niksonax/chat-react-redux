import React from "react";
import moment from "moment";
import "./Header.css";

class Header extends React.Component {
  getLastDate(messages) {
    let lastDateTime = 0;
    for (const message of messages) {
      const messageDate = new Date(message.createdAt);
      if (messageDate.getTime() > lastDateTime) {
        lastDateTime = messageDate.getTime();
      }
    }
    const lastDate = new Date(lastDateTime);
    moment.locale("uk");
    const lastDateFormat = moment(lastDate).format("DD.MM.yyyy HH:mm");
    return lastDateFormat;
  }

  render() {
    const allMessages = this.props.messages;
    const usersCount = this.props.users.length + 1; // users + current user
    const messagesCount = allMessages.length;
    const date = this.getLastDate(allMessages);

    return (
      <div className="header">
        <div className="header-title">Chat title</div>
        <div className="header-users">
          <span className="header-users-count">{usersCount}</span>
          participants
        </div>
        <div className="header-messages">
          <span className="header-messages-count">{messagesCount}</span>
          messages
        </div>

        <div className="header-last-message-date">{date}</div>
      </div>
    );
  }
}

export default Header;
