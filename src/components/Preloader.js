import React from "react";
import "./Preloader.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSpinner } from "@fortawesome/free-solid-svg-icons";

class Preloader extends React.Component {
  render() {
    return (
      <div className="preloader">
        <FontAwesomeIcon icon={faSpinner} className="icon" />
      </div>
    );
  }
}

export default Preloader;
