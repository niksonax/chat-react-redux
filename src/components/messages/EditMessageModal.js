import React from "react";
import { connect } from "react-redux";
import "./EditMessageModal.css";
import { toggleEditModal, setMessages } from "../../actions/actions";

class EditMessageModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      text: "",
    };
  }

  findMessageById(id) {
    if (id === 0) return { text: "" };
    const messages = this.props.chat.messages;
    for (const message of messages) {
      if (message.id === id) return message;
    }
  }

  componentDidMount() {
    const message = this.findMessageById(this.props.chat.modal.messageId);
    this.setState({ text: message.text });

    window.addEventListener("keyup", (e) => {
      if (e.key === "Enter" && !e.shiftKey) {
        this.onSubmitModal();
      }
    });
  }

  onChangeText(e) {
    this.setState({ text: e.target.value });
  }

  onSubmitModal() {
    const messages = this.props.chat.messages;
    for (const message of messages) {
      if (message.id === this.props.chat.modal.messageId) {
        message.text = this.state.text;
      }
    }
    this.props.setMessages(messages);
    this.onCloseModal();
  }

  onCloseModal() {
    this.props.toggleEditModal(false);
  }

  render() {
    if (this.props.chat.editModal === false) return null;

    return (
      <div
        className={`edit-message-modal ${
          this.props.chat.editModal ? "modal-shown" : ""
        }`}
      >
        <textarea
          className="edit-message-input"
          value={this.state.text}
          onChange={(e) => this.onChangeText(e)}
        ></textarea>
        <div className="edit-message-buttons">
          <button
            className="edit-message-close"
            onClick={() => this.onCloseModal()}
          >
            Close
          </button>
          <button
            className="edit-message-button"
            onClick={() => this.onSubmitModal()}
          >
            Edit
          </button>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    chat: state.chat,
  };
};

const mapDispatchToProps = {
  toggleEditModal,
  setMessages,
};

export default connect(mapStateToProps, mapDispatchToProps)(EditMessageModal);
