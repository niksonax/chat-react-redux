import React from "react";
import { connect } from "react-redux";
import { v4 as uuidv4 } from "uuid";
import "./MessageInput.css";
import { setMessages } from "../../actions/actions";

class MessageInput extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      message: "",
    };
  }

  textarea = React.createRef();

  createMessage = () => {
    return {
      id: uuidv4(),
      userId: this.props.userId,
      text: this.state.message,
      createdAt: new Date(Date.now()).toString(),
    };
  };

  onChangeHandler = (e) => {
    const string = e.target.value;
    if (string.substr(string.length - 1) === "\n" && !e.shiftKey) return;
    this.setState({ message: e.target.value });
  };

  onClickHandler = () => {
    this.textarea.current.value = "";
    this.textarea.current.focus();
  };

  sendMessageHandler = () => {
    if (this.state.message === "" || this.state.message === "\n") return;
    const messages = this.props.chat.messages;
    const message = this.createMessage();
    messages.push(message);
    this.props.setMessages(messages);
    this.setState({ message: "" });
  };

  componentDidMount = () => {
    window.addEventListener("keyup", (e) => {
      if (e.key === "Enter" && !e.shiftKey) {
        this.sendMessageHandler();
        this.onClickHandler();
      }
    });
  };

  render() {
    return (
      <div className="message-input">
        <textarea
          ref={this.textarea}
          className="message-input-text"
          onChange={(e) => this.onChangeHandler(e)}
          disabled={this.props.chat.editModal}
        ></textarea>
        <button
          className="message-input-button"
          onClick={() => {
            this.sendMessageHandler();
            this.onClickHandler();
          }}
        >
          Send
        </button>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    chat: state.chat,
  };
};

const mapDispatchToProps = {
  setMessages,
};

export default connect(mapStateToProps, mapDispatchToProps)(MessageInput);
