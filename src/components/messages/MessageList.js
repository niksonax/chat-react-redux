import React from "react";
import { connect } from "react-redux";
import "./MessageList.css";
//import {} from "../../actions/actions";
import Message from "./Message";
import OwnMessage from "./OwnMessage";

class MessageList extends React.Component {
  messagesEndRef = React.createRef();

  scrollToBottom = () => {
    this.messagesEndRef.current.scrollIntoView({ behavior: "smooth" });
  };

  componentDidMount() {
    this.scrollToBottom();
  }

  componentDidUpdate() {
    this.scrollToBottom();
  }

  render() {
    const messages = this.props.chat.messages;

    const sortedMessages = messages.sort((firstMessage, secondMessage) => {
      const fstMsgTime = new Date(firstMessage.createdAt).getTime();
      const scndMsgTime = new Date(secondMessage.createdAt).getTime();
      return fstMsgTime - scndMsgTime;
    });

    const messagesElements = sortedMessages.map((message) => {
      if (message.userId === this.props.userId) {
        return (
          <OwnMessage
            key={message.id}
            id={message.id}
            text={message.text}
            createdAt={message.createdAt}
          />
        );
      }
      return (
        <Message
          key={message.id}
          id={message.id}
          userId={message.userId}
          user={message.user}
          avatar={message.avatar}
          text={message.text}
          createdAt={message.createdAt}
        />
      );
    });

    return (
      <div className="message-list">
        {messagesElements}
        <div className="message-divider"></div>
        <br ref={this.messagesEndRef} />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    chat: state.chat,
  };
};

//const mapDispatchToProps = {};

export default connect(mapStateToProps)(MessageList);
