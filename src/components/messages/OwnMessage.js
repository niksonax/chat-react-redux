import React from "react";
import { connect } from "react-redux";
import moment from "moment";
import "./OwnMessage.css";
import {
  setMessages,
  setEditingMessageId,
  toggleEditModal,
} from "../../actions/actions";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrashAlt, faEdit } from "@fortawesome/free-solid-svg-icons";

class OwnMessage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isEditing: false,
      text: this.props.text,
    };
  }

  onMessageEdit = () => {
    this.setState({ isEditing: true });
    this.props.setEditingMessageId(this.props.id);
    this.props.toggleEditModal(true);
  };

  onMessageDelete = () => {
    const messages = this.props.chat.messages;
    const newMessages = messages.filter((msg) => msg.id !== this.props.id);
    this.props.setMessages(newMessages);
  };

  render() {
    const date = new Date(this.props.createdAt);
    const time = moment(date).format("HH:mm");

    return (
      <div className="own-message">
        <div className="message-time">{time}</div>
        <div className="message-text">{this.props.text}</div>
        <div className="message-tools">
          <span className="message-edit" onClick={() => this.onMessageEdit()}>
            <FontAwesomeIcon icon={faEdit} />
          </span>
          <span
            className="message-delete"
            onClick={() => this.onMessageDelete()}
          >
            <FontAwesomeIcon icon={faTrashAlt} />
          </span>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    chat: state.chat,
  };
};

const mapDispatchToProps = {
  setMessages,
  setEditingMessageId,
  toggleEditModal,
};

export default connect(mapStateToProps, mapDispatchToProps)(OwnMessage);
