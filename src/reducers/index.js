const initialState = {
  chat: {
    messages: [],
    users: [],
    currentUserId: 0,
    editModal: false,
    modal: {
      messageId: 0,
    },
    preloader: true,
  },
};

function rootReducer(state = initialState, action) {
  console.log("reducer", state, action);
  switch (action.type) {
    case "SET_PRELOADER":
      const { isShown } = action.payload;
      return {
        chat: {
          ...state.chat,
          preloader: isShown,
        },
      };
    case "SET_MESSAGES":
      const { messages } = action.payload;
      return {
        chat: {
          ...state.chat,
          messages: messages,
        },
      };
    case "SET_USERS":
      const { users } = action.payload;
      return {
        chat: {
          ...state.chat,
          users: users,
        },
      };
    case "SET_CURRENT_USER_ID":
      const { id } = action.payload;
      return {
        chat: {
          ...state.chat,
          currentUserId: id,
        },
      };
    case "TOGGLE_EDIT_MODAL":
      const { isEdit } = action.payload;
      return {
        chat: {
          ...state.chat,
          editModal: isEdit,
        },
      };
    case "SET_EDITING_MESSAGE_ID":
      const { messageId } = action.payload;
      return {
        chat: {
          ...state.chat,
          modal: {
            messageId: messageId,
          },
        },
      };
    default:
      return state;
  }
}

export default rootReducer;
